import "./App.css";
import ListTesting from "./components/ListTesting";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ListTesting />
      </header>
    </div>
  );
}

export default App;
