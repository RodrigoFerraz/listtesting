import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ListTesting from "./index";

describe("When all things is ok", () => {
  test("Should name insert in list when the user capture a value on input and click on the button", async () => {
    render(<ListTesting />);

    const arquivedText = screen.getByRole("textbox");
    const arquivedButton = screen.getByRole("button");

    userEvent.type(arquivedText, "Rodrigo");
    userEvent.click(arquivedButton);

    userEvent.type(arquivedText, "olá mundo");
    userEvent.click(arquivedButton);

    userEvent.type(arquivedText, "amar sempre");
    userEvent.click(arquivedButton);

    userEvent.type(arquivedText, "estou apaixonado");
    userEvent.click(arquivedButton);

    const myList = await screen.findAllByRole("listitem");

    expect(myList).toHaveLength(4);
  });

  test("when the button is disabled", () => {
    render(<ListTesting />);
    expect(screen.getByRole("button")).toBeDisabled();
  });

  test("when the button is not disable and the value input insert", () => {
    render(<ListTesting />);
    userEvent.type(screen.getByRole("textbox"), "Rodrigo");
    expect(screen.getByRole("button")).not.toBeDisabled();
  });
});
