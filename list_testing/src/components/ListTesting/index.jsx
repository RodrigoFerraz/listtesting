import { TextField, Button } from "@material-ui/core";

import { useState } from "react";
const ListTesting = () => {
  const [listName, setListName] = useState([]);
  const [name, setName] = useState("");

  const handleClick = () => {
    setListName([...listName, name]);
  };

  return (
    <div>
      <TextField
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
        placeholder="Digite aqui!"
      ></TextField>
      <Button disabled={!name} onClick={handleClick}>
        Add
      </Button>
      <h2>Minha lista</h2>
      <ul>
        {listName.map((name, index) => (
          <li key={index}>{name}</li>
        ))}
      </ul>
    </div>
  );
};

export default ListTesting;
